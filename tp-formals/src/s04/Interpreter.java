package s04;

import java.util.HashMap;

public class Interpreter {
    private static Lexer lexer;
    // TODO - A COMPLETER (ex. 3)

    public static int evaluate(String e) throws ExprException {
        lexer = new Lexer(e);
        int res = parseExpr();
        // test if nothing follows the expression...
        if (lexer.crtSymbol().length() > 0)
            throw new ExprException("bad suffix");
        return res;
    }

    private static int parseExpr() throws ExprException {
        if (lexer.crtSymbol().length() <= 0) throw new ExprException("incomplete expression");
        int res = 0;
        if (Character.isWhitespace(lexer.crtSymbol().charAt(0)))
            throw new ExprException("starting with space not allowed");

        res += parseTerm();
        while (lexer.isMinus() || lexer.isPlus()) {
            if (lexer.isMinus()) {
                lexer.goToNextSymbol();
                res -= parseTerm();
            } else {
                lexer.goToNextSymbol();
                res += parseTerm();
            }
        }
        return res;
    }

    private static int parseTerm() throws ExprException {
        if (lexer.crtSymbol().length() <= 0) throw new ExprException("incomplete expression");
        int res = 0;
        res += parseFact();
        while (lexer.isStar() || lexer.isSlash()) {
            if (lexer.isStar()) {
                lexer.goToNextSymbol();
                res *= parseTerm();
            } else {
                lexer.goToNextSymbol();
                res /= parseTerm();
            }
        }
        return res;
    }

    private static int parseFact() throws ExprException {
        if (lexer.crtSymbol().length() <= 0) throw new ExprException("incomplete expression");
        int res = 0;
        int arg = 0;
        String ident = "";
        if (lexer.isOpeningParenth()) {
            lexer.goToNextSymbol();
            res += parseExpr();
            if (!lexer.isClosingParenth()) throw new ExprException("no closing parenthesis");
            lexer.goToNextSymbol();
        } else if (lexer.isIdent()) {
            ident += lexer.crtSymbol();
            lexer.goToNextSymbol();
            if (lexer.isOpeningParenth()) {
                lexer.goToNextSymbol();
                arg += parseExpr();
                if (!lexer.isClosingParenth()) throw new ExprException("no closing parenthesis");
                lexer.goToNextSymbol();
            }
            res += applyFct(ident, arg);
        } else {
            res += lexer.intFromSymbol();
            lexer.goToNextSymbol();
        }

        return res;
    }

    private static int applyFct(String fctName, int arg) throws ExprException {
        if (fctName.equals("sqrt") && arg < 0) throw new ExprException("invalid arg");
        return switch (fctName) {
            case "abs" -> Math.abs(arg);
            case "sqr" -> (arg * arg);
            case "cube" -> (arg * arg * arg);
            case "sqrt" -> (int) Math.round(Math.sqrt(arg));
            default -> throw new ExprException("invalid ident");
        };
    }
}
