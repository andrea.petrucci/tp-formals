package s03;

import java.util.HashSet;
import java.util.Set;

public class FSM {
    private int[][] transition;  // copy of the transition table
    private Set<Integer> acceptingStates;

    public FSM(int[][] transitions,        // also gives n and m
               Set<Integer> acceptingStates) {
        this.transition = transitions;
        this.acceptingStates = acceptingStates;
    }


    public boolean accepts(int[] word) {
        // ------ Simulation pseudo-code :
        // crtState = 0;
        // for each symbol s in word
        //   apply transition with s
        // return true if crtState is accepting
        int crtState = 0;

        for (int s : word) {
            crtState = transition[crtState][s];
        }

        if (acceptingStates.contains(crtState)) return true;
        return false;

    }

    public int nStates() {
        return transition.length;
    }

    public int nSymbols() {
        return transition[0].length;
    }

    // -----------------------------------------------------------
    public static void main(String[] args) {
        if (args.length == 0)
            args = new String[]{"abbab", "baab", "bbabbb"};
        int[][] transitions = {
                {1, 0},       // <-- from state 0, with symbol a,b,c...
                {2, 1},       // <-- from state 1, with symbol a,b,c...
                {2, 2}
        };
        Set<Integer> acceptingStates = new HashSet<>();
        acceptingStates.add(1);
        FSM autom = new FSM(transitions, acceptingStates);
        for (int i = 0; i < args.length; i++) {
            String letters = args[i];
            int[] word = new int[letters.length()];
            for (int j = 0; j < letters.length(); j++)
                word[j] = letters.charAt(j) - 'a';
            boolean res = autom.accepts(word);
            System.out.println(letters + " : " + res);
        }
    }
}
