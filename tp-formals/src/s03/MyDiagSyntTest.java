package s03;

/**
 * <p>Description: Tests the DiagSynt class </p>
 *
 * @author Stotzer
 * @version 1.0
 */

import java.util.Random;

public class MyDiagSyntTest {

    public static String generateCorrectWord(Random r, int maxNbr) {
        String word = "";
        int n1 = r.nextInt(maxNbr) + 1;
        int n2 = r.nextInt(maxNbr) + 1;
        for (int i = 0; i < n1; i++)
            word += 'i';
        word += '+';
        for (int i = 0; i < n2; i++)
            word += 'i';
        word += '=';
        for (int i = 0; i < n1 + n2; i++)
            word += 'i';
        return word;
    }

    public static boolean testWord(String word, boolean expectedResult) {
        boolean res = DiagSynt.isAccepted(word);
        if (expectedResult != res)
            if (expectedResult)
                System.out.println("'" + word + "' should be accepted");
            else
                System.out.println("'" + word + "' should be refused");
        return res;
    }

    public static boolean testCorrectWords(Random r, int maxNbr, int nbrOfTest) {
        boolean res = true;
        String word;

        // Following words should be accepted
        for (int i = 0; i < nbrOfTest; i++) {
            word = generateCorrectWord(r, maxNbr);
            res = testWord(word, true) && res;
        }
        return res;
    }

    public static boolean testWrongWords(Random r, int maxNbr, int nbrOfTest) {
        boolean res = true;
        res = !testWord("", false) && res;
        res = !testWord("=", false) && res;
        res = !testWord("i=i", false) && res;
        String word;

        // Following words should be refused
        for (int i = 0; i < nbrOfTest; i++) {
            word = generateCorrectWord(r, maxNbr);
            if (r.nextBoolean())
                word = word.substring(1 + r.nextInt(word.length()));
            else
                word = word.substring(0, 1 + r.nextInt(word.length() - 1));
            res = !testWord(word, false) && res;
        }
        return res;
    }

    public static void main(String[] args) {
        boolean res = true;
        int maxNbr = 10;
        int nbrOfTest = 100;
        Random r = new Random();
        res = testWrongWords(r, maxNbr, nbrOfTest);
        res = testCorrectWords(r, maxNbr, nbrOfTest) && res;
        if (res)
            System.out.println("\nTest passed successfully");
        else
            System.out.println("\nTest failed; there is a bug");
    }
}
